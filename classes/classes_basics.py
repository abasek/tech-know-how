# CONSTRUCTOR CREATES INSTANCE VARIABLES
class DriveBot:
    def __init__(self):
        self.motor_speed = 0
        self.direction = 0


# MANUALLY CHANGING THE OBJECT VARIABLES
test_bot = DriveBot()
test_bot.motor_speed = 30
test_bot.direction = 90


# CHANGING OBJECT VARIABLES BY METHOD
def control_bot(self, new_speed, new_direction):
    self.motor_speed = new_speed
    self.direction = new_direction


# CONSTRUCTOR CREATES VARIABLES BASED ON PARAMETERS OR SET DEFAULTS
class DriveBotI:
    def __init__(self, motor_speed=0, direction=180):
        self.motor_speed = motor_speed
        self.direction = direction


# ASSIGNING NEW VARIABLES TO ALL THE OBJECTS
class DriveBot:
    all_disabled = False  # newly created variable
    latitude = -999999  # newly created variable

    def __init__(self, motor_speed=0, direction=180, sensor_range=10):
        self.motor_speed = motor_speed
        self.direction = direction
        self.sensor_range = sensor_range


DriveBot.latitude = 40.60793  # assigns variable to all the objects
DriveBot.all_disabled = False  # assigns variable to all the objects


# CHANGING THE CLASS VARIABLE BY OBJECT CREATION
class DriveBot:
    all_disabled = False
    latitude = -999999
    robot_count = 0  # newly created variable

    def __init__(self, motor_speed=0, direction=180):
        self.motor_speed = motor_speed
        self.direction = direction
        DriveBot.robot_count += 1  # referring to class variable - incrementing by 1
        self.id = DriveBot.robot_count  # referring to new object variable by copying the class var


# REFERRING TO OTHER CLASS VARIABLE IF AS ARGUMENT IN OUR CLASS METHOD IS AN OBJECT OF THAT CLASS
# CREATING OBJECT OF A CLASS REFERRING TO OTHER CLASS
# RETURNING OBJECT OF A SUBCLASS
class Franchise:

    def __init__(self, address, menus):  # menus are objects of a class Menu
        self.address = address
        self.menus = menus

    def __repr__(self):
        return "The address of the restaurant is {}".format(self.address)

    def available_menus(self, time):
        available_menu = []
        for menu in self.menus:
            if menu.start_time <= time <= menu.end_time:  # referring to variable of an object from Menu class
                available_menu.append(menu)
        return available_menu


class Menu:
    def __init__(self, name, items, start_time, end_time):
        self.name = name
        self.items = items
        self.start_time = start_time
        self.end_time = end_time

    def __repr__(self):
        return "{} menu available from {} to {}".format(self.name, self.start_time, self.end_time)


brunch = Menu("brunch", {'pancakes': 7.50, 'orange juice': 3.50}, 11, 16)
early_bird = Menu("Early-bird", {'salumeria plate': 8.00, 'espresso': 3.00}, 15, 18)
flagship_store = Franchise("1232 West End Road", [brunch, early_bird])

# print(flagship_store.available_menus(12))  # returns available menu
print(flagship_store.menus)  # returns all menus
print(flagship_store.menus[0])  # returns first menu object
