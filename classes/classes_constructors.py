#  __init__ - initializes newly created object. It's called every time the class is instantiated
#  __repr__ - changing the return value from 'object' to string

class Menu:
    def __init__(self, name, items, start_time, end_time):
        self.name = name
        self.items = items
        self.start_time = start_time
        self.end_time = end_time

    def __repr__(self):
        return "{} menu available from {} to {}".format(self.name, self.start_time, self.end_time)


#  __main__ - tells where to start the code execution and which part of the code should be executed

# #  BASIC APPROACH
# if __name__ == '__main__':
#     user_cart = get_new_user_cart('adrian@example.com')
#     user_cart["items"] = generate_cart_items(5)
#
#
# # BETTER APPROACH
# def main():
#     data = "My data read from the Web"
#     print(data)
#     modified_data = process_data(data)
#     print(modified_data)
#
#
# if __name__ == "__main__":
#     main()
