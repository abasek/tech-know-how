a_string = "Cool String"
an_int = 12
print(type(a_string))  # returns "<class 'str'>"
print(type(an_int))  # returns "<class 'int'>"


# A class is a template for a data type. It describes the kinds of information that class will hold and how a
# programmer will interact with that data.

# BASIC CLASS
class Facade:  # defining a class
    pass


facade_1 = Facade()  # creating an instance of the class
# by adding parentheses to name of the class we create an object
# then we assign that new instance to the variable
# Facade() is an object
# Instantiation takes a class and turns it into an object

print(type(facade_1))  # returns <class '__main__.Facade'>


# type() function does the opposite - returns the class that the object is an instance of
# __main__ means "this current file that we are running


# BASIC CLASS WITH VARIABLE
class Musician:
    title = "Rockstar"  # variable will be available for every instance of the class


drummer = Musician()  # drummer is instantiated object of type Musician
print(drummer.title)  # returns "Rockstar" (object.variable syntax)


# METHODS IN CLASSES
# Method is a function that is defined as part of a class
# self argument is an object that is calling the method
class Dog:
    dog_time_dilation = 7

    def time_explanation(self):  # self refers to an object - Dog()
        print("Dogs experience {} years for every 1 human year.".format(self.dog_time_dilation))
        # self refers to an object - Dog()


pipi_pitbull = Dog()  # object that is calling the method
pipi_pitbull.time_explanation()  # Returns "Dogs experience 7 years for every 1 human year."


# METHODS IN CLASS WITH AN ARGUMENT
class Circle:
    pi = 3.14

    def area(self, radius):
        return self.pi * radius ** 2


circle = Circle()

pizza_area = circle.area(12)
print(pizza_area)


# CONSTRUCTORS - methods used to prepare an object being instantiated
# dunder methods - so-named due to double-underscore abbreviated to "dunder"
# __init__ - initializes newly created object. It's called every time the class is instantiated

class Shouter:
    def __init__(self, phrase):
        if type(phrase) == str:
            print(phrase.upper())


shout1 = Shouter("shout")  # returns "SHOUT"
shout2 = Shouter("shout")  # returns "SHOUT"
shout3 = Shouter("let it all out")  # returns "LET IT ALL OUT"


# CONSTRUCTOR EXAMPLE
class Circle:
    pi = 3.14

    # Add constructor here:
    def __init__(self, diameter):
        print("New circle with diameter: {diameter}".format(diameter=diameter))


teaching_table = Circle(36)


# INSTANCE VARIABLES (OBJECT'S VARIABLES)
class FakeDict:
    pass


fake_dict1 = FakeDict()  # initializes object
fake_dict2 = FakeDict()  # initializes object

fake_dict1.fake_key = "This works!"  # 1 object variable
fake_dict2.fake_key = "This too!"  # 2 object variable

working_string = "{} {}".format(fake_dict1.fake_key, fake_dict2.fake_key)
print(working_string)  # returns "This works! This too!"


# CHECKING IF AN OBJECT HAS AN ATTRIBUTE USING hadattr(object, "attribute")
can_we_count_it = [{'s': False}, "sassafrass", 18, ["a", "c", "s", "d", "s"]]

for element in can_we_count_it:
    if hasattr(element, "count"):  # returns True if an object has a given attribute, False otherwise
        print(str(type(element)) + " has the count attribute!")
    else:
        print(str(type(element)) + " does not have the count attribute :(")

# CHECKING WHAT AN ATTRIBUTE HAS AN OBJECT USING getattr(object, "attribute", default)
# returns the object value and attribute, plus default if doesn't have an attribute
# getattr(attributeless, "other_fake_attribute", 800)  # returns 800, the default value

# CHECKING WHAT ATTRIBUTES HAS AN OBJECT USING dir
fun_list = [10, "string", {'abc': True}]
type(fun_list)  # returns <class 'list'>
dir(fun_list)  # returns ['__add__', '__class__', [...], 'append', 'clear', 'copy', 'count', 'extend', 'index',


# 'insert', 'pop', 'remove', 'reverse', 'sort']


# PRETTY FULL CLASS:
class Circle:
    pi = 3.14

    def __init__(self, diameter):
        self.radius = diameter / 2
        print("Creating circle with diameter {d}".format(d=diameter))

    def circumference(self):
        return 2 * Circle.pi * self.radius


medium_pizza = Circle(12)
print(medium_pizza.circumference())


# CHANGING THE RETURN VALUE FROM "object" TO STRING USING __report__() WHEN CALLING AN OBJECT
# __repr__ - tells what we want the string representation of the class to be. Must return a string
class Circle:
    pi = 3.14

    def __init__(self, diameter):
        self.radius = diameter / 2

    def __repr__(self):
        return "Circle with radius {}".format(self.radius)


medium_pizza = Circle(12)
teaching_table = Circle(36)
round_room = Circle(11460)

print(medium_pizza)  # if __repr__ doesn't exist then returns <__main__.Circle object at 0x01CF9FB0>
print(teaching_table)  # if exists returns Circle with radius 18.0
print(round_room)


# REFERRING TO OTHER CLASS VARIABLE IF AS ARGUMENT IN OUR CLASS METHOD IS AN OBJECT OF THAT CLASS
class Franchise:
    def __init__(self, address, menus):  # menus are objects of a class Menu
        self.address = address
        self.menus = menus

    def __repr__(self):
        return "The address of the restaurant is {}".format(self.address)

    def available_menus(self, time):
        available_menu = []
        for menu in self.menus:
            if menu.start_time <= time <= menu.end_time:  # referring to variable of an object from Menu class
                available_menu.append(menu)
        return available_menu


class Menu:
    def __init__(self, name, items, start_time, end_time):
        self.name = name
        self.items = items
        self.start_time = start_time
        self.end_time = end_time

    def __repr__(self):
        return "{} menu available from {} to {}".format(self.name, self.start_time, self.end_time)


brunch = Menu("brunch", {
  'pancakes': 7.50, 'waffles': 9.00, 'burger': 11.00, 'home fries': 4.50, 'coffee': 1.50, 'espresso': 3.00, 'tea': 1.00, 'mimosa': 10.50, 'orange juice': 3.50}, 11, 16)
early_bird = Menu("Early-bird", {
    'salumeria plate': 8.00, 'salad and breadsticks (serves 2, no refills)': 14.00, 'pizza with quattro formaggi': 9.00,
    'duck ragu': 17.50, 'mushroom ravioli (vegan)': 13.50, 'coffee': 1.50, 'espresso': 3.00}, 15, 18)
flagship_store = Franchise("1232 West End Road", [brunch, early_bird])

print(flagship_store.available_menus(12))


# RETURNING OBJECT IF IS PART OF THE CLASS OF ANOTHER CLASS
class Business:
    def __init__(self, name, franchises):
        self.name = name
        self.franchises = franchises


class Franchise:
    def __init__(self, address, menus):  # menus are objects of a class Menu
        self.address = address
        self.menus = menus

    def __repr__(self):
        return "The address of the restaurant is {}".format(self.address)

    def available_menus(self, time):
        available_menu = []
        for menu in self.menus:
            if menu.start_time <= time <= menu.end_time:  # referring to variable of on object from Menu class
                available_menu.append(menu)
        return available_menu


class Menu:
    def __init__(self, name, items, start_time, end_time):
        self.name = name
        self.items = items
        self.start_time = start_time
        self.end_time = end_time

    def __repr__(self):
        return "{} menu available from {} to {}".format(self.name, self.start_time, self.end_time)


brunch = Menu("brunch", {
    'pancakes': 7.50, 'waffles': 9.00, 'burger': 11.00, 'home fries': 4.50, 'coffee': 1.50, 'espresso': 3.00,
    'tea': 1.00, 'mimosa': 10.50, 'orange juice': 3.50}, 11, 16)

early_bird = Menu("Early-bird", {
    'salumeria plate': 8.00, 'salad and breadsticks (serves 2, no refills)': 14.00, 'pizza with quattro formaggi': 9.00,
    'duck ragu': 17.50, 'mushroom ravioli (vegan)': 13.50, 'coffee': 1.50, 'espresso': 3.00}, 15, 18)

aperas_menu = Menu("aperas", {
    'arepa pabellon': 7.00, 'pernil arepa': 8.50, 'guayanes arepa': 8.00, 'jamon arepa': 7.50
}, 10, 20)

flagship_store = Franchise("1232 West End Road", [brunch, early_bird])
new_installment = Franchise("12 East Mulberry Street", [brunch, early_bird])
aperas_place = Franchise("189 Fitzgerald Avenue", [aperas_menu])
basta_fazoolin = Business("Basta Fazoolin with my Heart", [flagship_store, new_installment])
arepa = Business("'Take a' Arepa", [aperas_place])

print(arepa.franchises[0])
print(arepa.franchises[0].menus[0])  # returning menu of aperas franchise and arepa business
