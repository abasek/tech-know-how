from datetime import datetime

# MAKING TIMESTAMP AN ID
order_id = datetime.timestamp(datetime.now())


# Returning specific part of the date
birthday = datetime(1989, 11, 12, 23, 5, 12)
# Running birthday.year in console will return 1989

# Returning the current date
datetime.now()
print(datetime.now)

# Returning number of days between dates
datetime(2022, 5, 13) - datetime(2017, 1, 1)

# Returning number of days etc. between now and some date
datetime.now() - datetime(2018, 1, 1)

# Parsing dates from a string formats into the date format
parsed_date = datetime.strptime('Jan 15, 2018', '%b, %d, %Y')
# Running parsed.date.month in a console will return in terminal 1
# characters can we found in docs.python.org

# Parsing date from a date formal into a string format
date_string = datetime.strftime(datetime.now(), '%b %d, %Y')
# Running date_string in terminal will result in string 'Now 15, 2018
