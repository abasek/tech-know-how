languages = ['Python', 'Java', 'JavaScript']
enumerate_prime = enumerate(languages)  # convert enumerate object to list
print(list(enumerate_prime))  # Output: [(0, 'Python'), (1, 'Java'), (2, 'JavaScript')]

grocery = ['bread', 'milk', 'butter']
enumerateGrocery = enumerate(grocery)
print(type(enumerateGrocery))  # returns <class 'enumerate'>
print(list(enumerateGrocery))  # [(0, 'bread'), (1, 'milk'), (2, 'butter')]
enumerateGrocery = enumerate(grocery, 10)
print(list(enumerateGrocery))  # [(10, 'bread'), (11, 'milk'), (12, 'butter')]

# LOOPING OVER AN ENUMERATE OBJECT
grocery = ['bread', 'milk', 'butter']
for item in enumerate(grocery):
    print(item)
# (0, 'bread')
# (1, 'milk')
# (2, 'butter')

for count, item in enumerate(grocery):
    print(count, item)
# 0 bread
# 1 milk
# 2 butter

for count, item in enumerate(grocery, 100):
    print(count, item)
# 100 bread
# 101 milk
# 102 butter
