# METHOD AND FUNCTIONS BELOW:
# max()
# min()
# round()

# MAX MIN ROUND
tshirt_price = 9.75
shorts_price = 15.50
mug_price = 5.99
poster_price = 2.00

max_price = max(tshirt_price, shorts_price, mug_price, poster_price)  # returns the biggest
min_price = min(tshirt_price, shorts_price, mug_price, poster_price)  # returns the lowest
rounded_price = round(tshirt_price, 1)  # returns rounded to 1 place after comma
print(max_price)
print(min_price)
print(rounded_price)


# SQUARED ROOT
def square_root(num):
    return num ** 0.5


# EXPONENT
def tenth_power(num):
    return num ** 10
