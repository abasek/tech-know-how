# RANDOM NUMBER BETWEEN XY
import random

random_number = random.randint(1, 9)
print(random_number)

# modules random.randint random.choice
# import random

# CREATING RANDOM COMPREHENSIVE LIST WITH RANDOM.RANDINT
random_list = [random.randint(1, 100) for i in range(101)]

# ASSIGNING RANDOM ELEMENT OF THE LIST USING RANDOM.CHOICE
random_number = random.choice(random_list)

print(random_list)
print(random_number)

# CREATING RANDOM LISTS
numbers_b = random.sample(range(1000), 12)
print(numbers_b)  # returns random 12 numbers out of 1000
