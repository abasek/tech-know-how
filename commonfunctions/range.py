# RANGE FUNCTION WORKING WITHOUT A LIST

my_range = range(10)
print(my_range)  # will return "range(0, 10)"

zero_to_seven = range(0,8)
print(zero_to_seven)  # will return "range(0, 8)"

# RANGE FUNCTIONS WORKING WITH A LIST
zero_to_seven_list = list(range(0, 8))  # will create a list [0, 1, 2, 3, 4, 5, 6, 7]
print(zero_to_seven_list)

zero_to_eight = range(0, 9)
print(list(zero_to_eight))  # will return [0, 1, 2, 3, 4, 5, 6, 7, 8]

skips_every_two = range(2, 9, 2)
print(list(skips_every_two))  # will return [2, 4, 6, 8]

my_range3 = range(1, 100, 10)
print(list(my_range3))  # will return [1, 11, 21, 31, 41, 51, 61, 71, 81, 91]
