# IF STATEMENT - if it's True then run
if 2 == 4 - 2:
    print("apple")

# ELSE STATEMENT
if (120 >= 120) and (1.9 >= 2.0):
    print("You meet the reqs!")
else:
    print("You do not meet the reqs.")

# ELIF STATEMENT (example below)
grade = 86
if grade >= 90:
    print("A")
elif grade >= 80:
    print("B")
else:
    print("D")

# FOR LOOP
ingredients = ["asd", "sdf", "dfg"]
for i in ingredients:
    print(i)

# FOR LOOP WITH _ USAGE
for _ in range(5):
    print("five")

board_games = ["Settlers of Catan", "Carcassone", "Power Grid", "Agricola", "Scrabble"]
for game in board_games:
    print(game)

# WHILE LOOP
count = 0
while count <= 3:
    print(count)
    count += 1
print("We have liftoff!")  # returns 11 times (from 10 to 0) plus string

# WHILE LOOP IN ELEGANT FORMAT
count = 0
while count <= 3: print(count); count += 1

# INFINITE LOOP - Stop icon will stop running or CTRL + C stops it (where?)
my_favorite_numbers = [4, 8, 15, 16, 42]
for number in my_favorite_numbers:
    my_favorite_numbers.append(1)

# FOR LOOP WITH IF AND BRAKE
items_on_sale = ["blue shirt", "striped socks", "knit dress", "red headband", "dinosaur onesie"]
for item in items_on_sale:
    print(item)
    if item == "knit dress":
        break
print("End of search!")

# FOR LOOP WITH CONTINUE
ages = [12, 38, 34, 26, 21, 19, 67, 41, 17]
for i in ages:
    if i < 21:
        continue
    print(i)

# NESTED LOOPS WITH 2D LIST
sales_data = [[12, 17, 22], [2, 10, 3], [5, 12, 13]]
scoops_sold = 0
for location in sales_data:
    for scoop in location:
        scoops_sold += scoop
print(scoops_sold)  # returns sum of sales

# LIST COMPREHENSIONS
# new_list = [<expression> for <element> in <collection>]
numbers = [2, -1, 79, 33, -45]
doubled = []
for number in numbers:
    doubled.append(number * 2)  # regular way
doubled = [num * 2 for num in numbers]  # list comprehension

# LIST COMPREHENSION WITH IF
numbers = [2, -1, 79, 33, -45]
only_negative_doubled = []
for num in numbers:
    if num < 0:
        only_negative_doubled.append(num * 2)  # regular way
negative_doubled = [num * 2 for num in numbers if num < 0]  # list comprehension

# LIST COMPREHENSION WITH NESTED LOOP
my_list = []
for x in [20, 40, 60]:
    for y in [2, 4, 6]:
        my_list.append(x * y)
my_list = [x * y for x in [20, 40, 60] for y in [2, 4, 6]]

# LIST COMPREHENSION IF ELSE
obj = ["Even" if i % 2 == 0 else "Odd" for i in range(10)]


# DIVISIBLE BY TEN
def divisible_by_ten(num):
    if num % 10 == 0:
        return True
    else:
        return False


# ALWAYS FALSE
def always_false(num):
    if num > num - 1:
        return False


def always_false_2(num):
    if num > 0 > num:  # same as   if (num > 0 and num < 0):
        return True
    else:
        return False


# IF ELIF EXAMPLE
def movie_review(rating):
    if rating <= 5:
        return "Avoid at all costs!"
    elif 5 < rating < 9:  # same as rating > 5 and rating < 9:
        return "This one was fun."
    elif rating >= 9:
        return "Outstanding!"


# TRY EXCEPT ON A DICTIONARY
caffeine_level = {"espresso": 64, "chai": 40, "decaf": 0, "drip": 120}
try:
    print(caffeine_level["matcha"])
except KeyError:
    print("Unknown Caffeine Level")


# TRY EXCEPT ELSE FINALLY:
def divide(x, y):
    try:
        result = x // y
    except ZeroDivisionError:
        print("Sorry ! You are dividing by zero ")
    else:
        print("Yeah ! Your answer is :", result)
    finally:
        print('This is always executed')  # this block is always executed, regardless of exception generation


divide(3, 2)  # returns "Yeah ! Your answer is : 1" and "This is always executed"
divide(3, 0)  # returns "Sorry ! You are dividing by zero " and "This is always executed"


# BREAK STATEMENT IN FOR LOOP
dog_breeds_available_for_adoption = ['french_bulldog', 'dalmation', 'shihtzu', 'poodle', 'collie']
dog_breed_I_want = 'dalmation'

for dog in dog_breeds_available_for_adoption:
    if dog == dog_breed_I_want:
        break
print("They have the dog I want!")

# CONTINUE STATEMENT IN FOR LOOP
ages = [12, 38, 34, 26, 21, 19, 67, 41, 17]

for age in ages:
    if age < 21:
        continue
    print(age)  # prints only ages above 21


# IF STATEMENT FOR ALL ELEMENTS IS TRUE PRINT TRUE, IF 1 IS NOT PRINT FALSE
def x_length_words(sentence, x):
    words = sentence.split(" ")
    for word in words:
        if len(word) < x:
            return False
    return True
