# PRINTING X TIMES - FOR LOOP WITH RANGE
promise = "I will finish the python loops module!"
for i in range(5):
    print(promise)  # will print 5 times

for temp in range(6):
    print("Loop is on iteration number " + str(temp + 1))  # to see the iterations

# LISTING A LIST ITEMS WITH CONDITION - FOR LOOP WITH CONTINUE
ages = [12, 38, 34, 26, 21, 19, 67, 41, 17]
for i in ages:
    if i < 21:
        continue
    print(i)


# RETURNING TRUE IF STATEMENT FOR ALL LIST  ELEMENTS IS TRUE, IF 1 IS NOT RETURN FALSE
def x_length_words(sentence, x):
    words = sentence.split(" ")
    for word in words:
        if len(word) < x:
            return False
    return True


# SUMS NUMBERS FROM 2D LIST - NESTED LOOPS WITH 2D LIST
sales_data = [[12, 17, 22], [2, 10, 3], [5, 12, 13]]
scoops_sold = 0
for location in sales_data:
    for scoop in location:
        scoops_sold += scoop
print(scoops_sold)  # returns sum of sales

# MATH FOR ALL LIST ELEMENTS
hairstyles = ["bouffant", "pixie", "dreadlocks", "crew", "bowl", "bob", "mohawk", "flattop"]
prices = [30, 25, 40, 20, 20, 35, 50, 35]
last_week = [2, 3, 5, 8, 4, 4, 6, 2]
total_revenue = 0

for i in range(0, len(hairstyles)):
    total_revenue += prices[i] * last_week[i]

# FILTERING LIST BASED ON ANOTHER LIST WITH LIST COMPREHENSION
hairstyles = ["bouffant", "pixie", "dreadlocks", "crew", "bowl", "bob", "mohawk", "flattop"]
prices = [30, 25, 40, 20, 20, 35, 50, 35]
new_prices = [i - 5 for i in prices]
cuts_under_30 = [hairstyles[i] for i in range(0, len(hairstyles)) if new_prices[i] < 30]


# COUNTING HOW MANY LIST ELEMENTS ARE DIVISIBLE BY 10
def divisible_by_ten(nums):
    count = 0
    for i in nums:
        if i % 10 == 0:
            count += 1
    return count


# MODIFYING ALL LIST ELEMENTS
def add_greetings(names):
    new_list = []
    for n in names:
        new_list.append("Hello " + n)
    return new_list


# ACCESSING ALL ODD LIST INDEX ELEMENTS AND CREATING THE NEW LIST WITH ODD INDEXES ONLY
def odd_indices(lst):
    new = []
    for i in range(1, len(lst), 2):
        new.append(lst[i])
    return new


# MATH ON EVERY LIST ELEMENT AGAINST EVERY OTHER LIST ELEMENT - NESTED LOOP
def exponents(bases, powers):
    new_list = []
    for b in bases:
        for p in powers:
            new_list.append(b ** p)
    return new_list


print(exponents([2, 3, 4], [1, 2, 3]))


# MATH ON EVERY LIST ELEMENT AGAINST SAME INDEX OTHER LIST ELEMENT
def same_values_index(lst1, lst2):
    new_lst = []
    for index in range(len(lst1)):  # FOR ALL LIST INDEXES
        new_lst.append(lst1[index] * lst2[index])
    return new_lst


# ACCESSING ALL LIST ELEMENTS - SUM OF ALL LIST ELEMENTS
def larger_sum(lst1):
    sum1 = 0
    for i in range(0, len(lst1)):
        sum1 += lst1[i]
    return lst1


# MATH UNTIL CONDITION MET USING BREAK - SUM LIST ELEMENTS UNTIL IT REACHES 9000
def over_nine_thousand(lst):
    sum_list = 0
    for i in lst:
        sum_list += lst[i]
        if sum_list > 9000:
            break
    return sum_list


# RETURNING THE LARGEST NUMBER FROM THE LIST
def max_num(nums):
    biggest = nums[0]
    for i in nums:
        if i > biggest:
            biggest = i
    return biggest


# COMPARING VALUES OF 2 LISTS (OF EQUAL SIZE) - NESTED LOOP
def same_values(lst1, lst2):
    new_list = []
    for i in lst1:  # FOR ALL LIST ELEMENTS
        for n in lst2:
            if i == n:
                new_list.append(i)
    return new_list


print(same_values([5, 1, -10, 3, 3], [5, 10, -10, 3, 5]))


# COMPARING INDEX VALUES OF 2 LISTS (OF EQUAL SIZE)
def same_values_index_2(lst1, lst2):
    new_lst = []
    for index in range(len(lst1)):  # FOR ALL LIST INDEXES
        if lst1[index] == lst2[index]:
            new_lst.append(index)
    return new_lst
