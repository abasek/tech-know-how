# WHILE LOOP WITH LIST
ingredients = ["milk", "sugar", "vanilla extract", "dough", "chocolate"]
length = len(ingredients)
index = 0

while index < length:
    print(ingredients[index])
    index += 1

# PRINTING UNTIL COUNT EQUALS 3
count = 0
while count <= 3:
    print(count)
    count += 1
print("We have liftoff!")  # returns 11 times (from 10 to 0) plus string

# PRINTING ALL LIST ELEMENTS
ingredients = ["milk", "sugar", "vanilla extract", "dough", "chocolate"]
length = len(ingredients)
index = 0

while index < length:
    print(ingredients[index])
    index += 1


# DELETE ITEMS FROM THE BEGINNING OF THE LIST UNTIL THE FIRST IS ODD
def delete_starting_evens(lst):
    while len(lst) > 0 and lst[0] % 2 == 0:
        lst.pop(0)
    else:
        return lst


# SAME AS ABOVE BUT DIFFERENT SOLUTION
def delete_starting_evens_2(lst):
    while len(lst) > 0 and lst[0] % 2 == 0:
        lst = lst[1:]
    return lst
