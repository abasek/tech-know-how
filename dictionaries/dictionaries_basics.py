# A DICTIONARY IS AN UNORDERED SET OF KEY: VALUE PAIRS
# LIST CAN BE VALUE IN A DICTIONARY, BUT NOT A KEY
# DICTIONARY CAN BE VALUE IN A DICTIONARY, BUT NOT A KEY
# LIST OF METHODS BELOW
# .update() - adds multiple items
# .get() - returns value if key exists, None if doesn't exist. Second parameter will be default if not found
# .pop() - returns value & deletes item. May returns default value if doesn't exists
# .list() - getting all the keys  in a form of a list
# .keys() - getting all the keys in a form of a view object enabling loops etc.
# .values() - getting all values in a form of a view object
# .items() - getting both in a form of a view object as a tuple (key, value)

# DICTIONARIES EXAMPLES
menu = {"avocado toast": 6, "carrot juice": 5, "blueberry muffin": 2}
subtotal_to_total = {20: 24, 10: 12, 5: 6, 15: 18}
students_in_classes = {"software design": ["Aaron", "Samson"], "cartography": ["Christopher", "Juan"]}
person = {"name": "Shuri", "age": 18, "family": ["T'Chaka", "Ramonda"]}
my_empty_dictionary = {}

# ADDING AN ITEM TO A DICTIONARY
# dictionary[key] = value
menu_food = {"oatmeal": 3, "avocado toast": 6, "carrot juice": 5}
menu_food["cheesecake"] = 8

# ADDING MULTIPLE ITEMS TO A DICTIONARY USING .UPDATE
sensors = {"living room": 21, "kitchen": 23, "bedroom": 20}
sensors.update({"pantry": 22, "guest room": 25, "patio": 34})

# CHANGING THE VALUE OF AN ITEM
menu_new = {"oatmeal": 3, "avocado toast": 6, "carrot juice": 5}
menu_new["oatmeal"] = 5

# RETURNING A VALUE OF AN ITEM
building_heights = {"Burj Khalifa": 828, "Shanghai Tower": 632}
print(building_heights["Burj Khalifa"])  # 828
print(building_heights["Palac Kultury"])  # KeyError

# RETURNING A VALUE OF AN ITEM USING .GET
user_ids = {"teraCoder": 100, "samTheJavaMaam": 200}
print(user_ids.get("teraCoder"))  # 100
print(user_ids.get("teraCoder", 300))  # 100
print(user_ids.get("Not Existing Key"))  # None
print(user_ids.get("Not Existing Key", 300))  # 300

# IF A KEY IN A DICTIONARY
inventory = {"iron spear": 12, "invisible knife": 30}
print("iron spear" in inventory)  # True
print(12 in inventory)  # False, as key 12 not found
print(inventory.get("iron spear"))  # 12
print(inventory.get(12))  # None

if "iron spear" in inventory:
    print(inventory["iron spear"])  # returns 12
if inventory.get("iron spear"):
    print(inventory["iron spear"])  # returns 12
if 12 in inventory:
    print(inventory["iron spear"])  # nothing
if inventory.get(12):
    print(inventory["iron spear"])  # nothing

# RETURNING VALUE & DELETING A KEY USING .POP
available_items = {"strength sandwich": 25, "stamina grains": 15, "power stew": 30}
health_points = 20
health_points += available_items.pop("stamina grains", 0)  # adds 0 if doesn't exists, deletes the key

# ASSIGNING VALUE FROM 1 DICT TO ANOTHER USING .POP
tarot = {1: "The Magician", 2: "The High Priestess", 13: "Death"}
spread = {}
spread["past"] = tarot.pop(13)

# RETURNING ALL KEYS IN A FORM OF A VIEW OBJECT USING .KEYS
user_ids = {"teraCoder": 100019, "pythonGuy": 182921, "samTheJavaMaam": 123112}
users = user_ids.keys()
print(users)  # returns dict_keys(['teraCoder', 'pythonGuy', 'samTheJavaMaam'])

# PRINT ALL KEYS FROM A DICTIONARY
user_ids = {"teraCoder": 100019, "pythonGuy": 182921, "samTheJavaMaam": 123112}

for user in user_ids.keys():
    print(user)

# RETURNING ALL VALUES IN A FORM OF A VIEW OBJECT USING .VALUES
test_scores = {"Grace": [80, 72, 90], "Jeffrey": [88, 68, 81], "Sylvia": [80, 82, 84]}
for score_list in test_scores.values():
    print(score_list)  # returns:
# [80, 72, 90]
# [88, 68, 81]
# [80, 82, 84]

# SUM UP ALL THE VALUES FROM A DICTIONARY
num_exercises = {"functions": 10, "syntax": 13, "control flow": 15}
total_exercises = 0

for e in num_exercises.values():
    total_exercises += e
print(total_exercises)

# RETURNING ALL BOTH KEY & VALUE IN A FORM OF A VIEW OBJECT USING .ITEMS
biggest_brands = {"Apple": 184, "Google": 141.7}
one_brand = biggest_brands.items()  # returns dict_items([('Apple', 184), ('Google', 141.7)])

# PRINT BOTH KEY & VALUES
pct_women_in_occupation = {"CEO": 28, "Engineering Manager": 9, "Pharmacist": 58}
for o, n in pct_women_in_occupation.items():
    print("Women make up " + str(n) + " percent of " + o + "s")

# WHAT IS THE OUTPUT OF THE FOLLOWING CODE:
oscars = {"Best Picture": "Moonlight", "Best Actor": "Casey Affleck"}
for element in oscars:
    print(element)  # returns keys, not paired (key, value)
    "Best Picture"
    "Best Actor"

# ASSIGNING VALUE FROM 1 DICT TO ANOTHER USING .POP
tarot = {1: "The Magician", 2: "The High Priestess", 13: "Death"}
spread = {}
spread["past"] = tarot.pop(13)
spread["present"] = tarot.pop(22)
spread["future"] = tarot.pop(10)

for k, v in spread.items():
    print("Your " + k + " is the " + v + " card.")

# CONVERTING 2 LISTS INTO A DICTIONARY - DICTIONARY COMPREHENSIONS
drinks = ["espresso", "chai", "decaf", "drip"]
caffeine = [64, 40, 0, 120]
zipped_drinks = zip(drinks, caffeine)  # creates iterator of tuples, can check by print(list(zipped_drinks))
drinks_to_caffeine = {key: value for key, value in zipped_drinks}

songs = ["Like a Rolling Stone", "Satisfaction", "Imagine", "What's Going On", "Respect", "Good Vibrations"]
playcounts = [78, 29, 44, 21, 89, 5]
plays = {key: value for key, value in zip(songs, playcounts)}

# RETURNING ALL KEYS IN A FORM OF A LIST USING .LIST()
test_scores = {"Grace": [80, 72, 90], "Jeffrey": [88, 68, 81], "Sylvia": [80, 82, 84]}
list(test_scores)  # returns ["Grace", "Jeffrey", "Sylvia"]

# RETURNING ALL VALUES IN A FORM OF A LIST USING .VALUES
test_scores = {"Grace": [80, 72, 90], "Jeffrey": [88, 68, 81], "Sylvia": [80, 82, 84]}
print(list(test_scores.values()))  # returns [[80, 72, 90], [88, 68, 81], [80, 82, 84]]

