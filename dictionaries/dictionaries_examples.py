# RETURN SUM OF VALUES OF ALL EVEN KEYS
def sum_even_keys(my_dictionary):
    total = 0
    for k, v in my_dictionary.items():
        if k % 2 == 0:
            total += v
    return total


# RETURN DICTIONARY WITH ALL VALUES INCREASED BY 10
def add_ten(my_dictionary):
    for key in my_dictionary.keys():
        my_dictionary[key] += 10
    return my_dictionary


# RETURN LIST OF VALUES THAT ARE ALSO KEYS (SEE ALSO BELOW)
def values_that_are_keys(my_dictionary):
    pairs = []
    keys = list(my_dictionary)
    values = list(my_dictionary.values())
    for k in keys:
        if k in values:
            pairs.append(k)
    return pairs


# RETURN LIST OF VALUES THAT ARE ALSO KEYS - EASIER WAY
def values_that_are_keys_2(my_dictionary):
    value_keys = []
    for value in my_dictionary.values():
        if value in my_dictionary:
            value_keys.append(value)
    return value_keys


# RETURN THE KEY ASSOCIATED WITH THE LARGEST VALUE IN DICT
def max_key(my_dictionary):
    biggest = 0
    for v in my_dictionary.values():
        if v > biggest:
            biggest = v
    for k in my_dictionary.keys():
        if my_dictionary[k] == biggest:
            return k


# RETURN DICTIONARY WITH NAME AS KEY AND ITS LENGTH AS VALUE BASED ON THE LIST OF WORDS
def word_length_dictionary(words):
    word_lengths = {}
    for word in words:
        word_lengths[word] = len(word)
    return word_lengths


# RETURN DICTIONARY CONTAINING THE FREQUENCY OF EACH ELEMENT IN THE LIST
def frequency_dictionary(words_new):
    dicto = {}
    for word_new in words_new:
        dicto[word_new] = words_new.count(word_new)
    return dicto


# RETURNS NUMBER OF UNIQUE VALUES IN DICTIONARY
def unique_values(my_dictionary):
    lista = []
    for k in my_dictionary.values():
        if k not in lista:
            lista.append(k)
    return len(lista)


# SUM UP WHO WON THE GAME (SCRABBLE)
letters = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V",
           "W", "X", "Y", "Z"]
points = [1, 3, 3, 2, 1, 4, 2, 4, 1, 8, 5, 1, 3, 4, 1, 3, 10, 1, 1, 1, 1, 4, 4, 8, 4, 10]
player_to_words = {"player1": ["BLUE", "TENNIS", "EXIT"], "wordNerd": ["EARTH", "EYES", "MACHINE"]}
player_to_points = {}  # here we expect to have the result

letter_to_points = {key: value for key, value in zip(letters, points)}  # creating a dictionary
letter_to_points[" "] = 0  # extending the dictionary for a space with 0 points


def score_word(word_x):
    point_total = 0
    for letter in word_x:
        point_total += letter_to_points[letter]
    else:
        point_total += 0
    return point_total  # returns sum of point for a dedicated word


for player, words in player_to_words.items():
    player_points = 0
    for word in words:
        player_points += score_word(word)
        player_to_points[player] = player_points

print(player_to_points)
