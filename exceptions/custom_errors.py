# RAISING IN-BUILT ERROR
def validate(name):
    if len(name) < 10:
        raise ValueError


validate('Adam')  # returns ValueError


# RAISING IN-BUILT ERROR WITH CUSTOM MESSAGE
def validate(name):
    if len(name) < 10:
        raise ValueError('Name to short')


validate('Adam')  # returns ValueError: Name to short


# # RAISING CUSTOM ERROR BASED ON VALUE ERROR:
class NameTooShortErrorV(ValueError):
    pass


def validate(name):
    if len(name) < 10:
        raise NameTooShortErrorV


validate('Adam')  # returns NameTooShortError


# RAISING CUSTOM ERROR BASED ON VALUE ERROR WITH VARIABLE:
class NameTooShortError(ValueError):
    pass


def validate(name):
    if len(name) < 10:
        raise NameTooShortError(name)


validate('Adam')  # returns NameTooShortError: Adam


# RAISING CUSTOM ERROR WITH VARIABLE
class NameTooShortErrorE(Exception):
    pass


def validate(name):
    if len(name) < 10:
        raise NameTooShortErrorE(name)


validate('Adam')  # returns NameTooShortError: Adam
