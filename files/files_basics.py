# .read() - returns all file as a string
# .readlines() - returns each line as a string
# .readline() - returns first line
# .DictReader()

# RETURNING A FILE CONTENT AS A STRING
with open("welcome.txt") as text_file:
    text_data = text_file.read()

print(text_data)

# RETURNING A FILE CONTENT AS A LINES
with open("how_many_lines.txt") as lines_doc:
    for line in lines_doc.readlines():
        print(line)  # returns each line separately

# RETURNING FIRST LINE OF THE CONTENT
with open("just_the_first.txt") as first_line_doc:
    first_line = first_line_doc.readline()
print(first_line)

# WRITING A FILE WITH A STRING
with open("bad_bands.txt", "w") as bad_bands_doc:  # "w" argument makes it open in 'write mode'
    bad_bands_doc.write("Some new string")  # "Some new string"

# APPENDING ADDITIONAL PART OF THE STRING TO THE SAME LINE
with open("bad_bands.txt", "w") as bad_bands_doc:
    bad_bands_doc.write("Some new string")
    bad_bands_doc.write("Another part")
with open("bad_bands.txt") as modified_doc:
    modified_text = modified_doc.read()
    print(modified_text)  # returns "Some new stringAnother part" - doesn't matter how many times we run the script!

# APPENDING ADDITIONAL PART OF THE STRING AS A NEW LINE
with open("cool_dogs.txt", "a") as cool_dogs_file:  # "a" makes it open in 'append mode'
    cool_dogs_file.write("Air Buddy")

with open("cool_dogs.txt") as cool_dogs_file:
    print(cool_dogs_file.read())  # returns "Air buddyAir buddyAir buddyAir" as many times as it was run

# WORKING WITH THE FILE IN AN OLD WAY
fun_cities_file = open('fun_cities.txt', 'a')  # opening the connection to a file
fun_cities_file.write("Montréal")
fun_cities_file.close()  # closing the connection to the file

# READING A CSV FILE USING .DictReader()
import csv

with open("cool_csv.csv") as cool_csv_file:
    cool_csv_dict = csv.DictReader(cool_csv_file)
    print(cool_csv_dict)  # returns <csv.DictReader object at 0x7f0640492da0>
    for row in cool_csv_dict:
        print(row["Cool Fact"])  # returns rows from the column "Cool Fact"

# READING A CSV FILE USING .DictReader() with newline AND delimiter
import csv

isbn_list = []
with open("books.csv", newline="") as books_csv:
    books_reader = csv.DictReader(books_csv, delimiter='@')
    for i in books_reader:
        isbn_list.append(i['ISBN'])

# WRITING A CSV FILE USING .DictWriter()
big_list = [{'name': 'Fredrick Stein', 'userid': 6712359021, 'is_admin': False},
            {'name': 'Wiltmore Denis', 'userid': 2525942, 'is_admin': False},
            {'name': 'Greely Plonk', 'userid': 15890235, 'is_admin': False},
            {'name': 'Dendris Stulo', 'userid': 572189563, 'is_admin': True}]

import csv
with open('output.csv', 'w') as output_csv:
    fields = ['name', 'userid', 'is_admin']
    output_writer = csv.DictWriter(output_csv, fieldnames=fields)  # instantiate CSV writer
    output_writer.writeheader()  # writes fields passed to fieldnames as the first row of the file
    for item in big_list:  # for dictionaries from a big_list with each field in 'fields' as keys
        output_writer.writerow(item)  # add row in instantiated CSV writer

# WRITING A CSV FILE USING .DictWriter() - OTHER EXAMPLE WITHOUT FIELDS
access_log = [{'time': '08:39:37', 'limit': 844404, 'address': '1.227.124.181'},
              {'time': '13:13:35', 'limit': 543871, 'address': '198.51.139.193'},
              {'time': '19:40:45', 'limit': 3021, 'address': '172.1.254.208'},
              {'time': '18:57:16', 'limit': 67031769, 'address': '172.58.247.219'},
              {'time': '21:17:13', 'limit': 9083, 'address': '124.144.20.113'},
              {'time': '23:34:17', 'limit': 65913, 'address': '203.236.149.220'},
              {'time': '13:58:05', 'limit': 1541474, 'address': '192.52.206.76'},
              {'time': '10:52:00', 'limit': 11465607, 'address': '104.47.149.93'},
              {'time': '14:56:12', 'limit': 109, 'address': '192.31.185.7'},
              {'time': '18:56:35', 'limit': 6207, 'address': '2.228.164.197'}]
fields = ['time', 'address', 'limit']

import csv
with open("logger.csv", "w") as logger_csv:
    log_writer = csv.DictWriter(logger_csv, fieldnames=fields)
    log_writer.writeheader()
    for log in access_log:
        log_writer.writerow(log)

# READING A JSON FILE USING .load()
import json

with open("message.json") as message_json:
    message = json.load(message_json)

print(message)  # returns {'text': "Now that's JSON!", 'secret text': "Now that's some _serious_ JSON!"}
print(message["text"])  # returns "Now that's JSON!"

# WRITING A JSON FILE USING .dump()
data_payload = [
    {'interesting message': 'What is JSON? A web application\'s little pile of secrets.',
     'follow up': 'But enough talk!'}
]

import json
with open("data.json", "w") as data_json:
    json.dump(data_payload, data_json)
