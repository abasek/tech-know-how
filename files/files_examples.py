# COPYING CSV COLUMN DATA INTO A LIST
import csv

isbn_list = []
with open("books.csv", newline="") as books_csv:
    books_reader = csv.DictReader(books_csv, delimiter='@')
    for i in books_reader:
        isbn_list.append(i['ISBN'])

# COPYING CSV COLUMN DATA INTO A LIST - ANOTHER EXAMPLE
import csv

compromised_users = []
with open("passwords.csv") as password_file:
    password_csv = csv.DictReader(password_file)
    for password in password_csv:
        compromised_users.append(password["Username"])

# COPYING A LIST OF STRINGS TO A TEXT FILE FROM A LIST
compromised_users = ['jean49', 'haydenashley', 'michaelastephens', 'denisephillips', 'andrew24', 'kaylaabbott',
                     'tmartinez', 'mholden', 'randygilbert', 'watsonlouis', 'mdavis', 'patrickprice', 'kgriffith',
                     'hannasarah', 'xaviermartin', 'hrodriguez', 'erodriguez', 'danielleclark', 'timothy26',
                     'elizabeth19']

with open("compromised_users.txt", "w") as compromised_users_file:
    for user in compromised_users:
        compromised_users_file.write(user)

# OVERWRITE CSV FILE WITH TEXT
with open("passwords.csv", "w") as new_passwords_obj:
    slash_null_sig = """
 _  _     ___   __  ____             
/ )( \   / __) /  \(_  _)            
) \/ (  ( (_ \(  O ) )(              
\____/   \___/ \__/ (__)             
 _  _   __    ___  __ _  ____  ____  
/ )( \ / _\  / __)(  / )(  __)(    \ 
) __ (/    \( (__  )  (  ) _)  ) D ( 
\_)(_/\_/\_/ \___)(__\_)(____)(____/ 
        ____  __     __   ____  _  _ 
 ___   / ___)(  )   / _\ / ___)/ )( \
(___)  \___ \/ (_/\/    \\___ \) __ (
       (____/\____/\_/\_/(____/\_)(_/
 __ _  _  _  __    __                
(  ( \/ )( \(  )  (  )               
/    /) \/ (/ (_/\/ (_/\             
\_)__)\____/\____/\____/
"""
    new_passwords_obj.write(slash_null_sig)  # overwriting the file with the string

with open("passwords.csv") as check:
    checker = check.read()
    print(checker)  # checking whether it was overwritten properly
