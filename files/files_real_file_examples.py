# # OPENING FILE FROM THE SAME DIRECTORY
# with open("Adam_Test.txt") as file:
#     text = file.read()
#     print(text)
#
# OPENING FILE FROM A DIFFERENT DIRECTORY & FIXING THE PATH
path = r"C:\Users\Admin\PycharmProjects\CodeAcademyPro2\Command Line\Linux Bash"
# r before the path converts normal string to raw string
with open(path) as file:
    text = file.read()
    print(text)
