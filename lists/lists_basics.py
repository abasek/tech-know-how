# STANDARD LISTS
heights = [61, 70, 67, 64, 65]
names = ["Noelle", "Ava", "Sam", "Mia"]
mixed_list_common = ["Mia", 27, False, 0.5]
empty_list = []

# APPENDING LIST ELEMENTS
append_example = ['This', 'is', 'an', 'example']
append_example.append('list')

example_list = [1, 2, 3, 4]
example_list.append(5)

# INSERTING A LIST ELEMENT TO SPECIFIC INDEX
# list.insert([index], [value])
pizzas_and_prices = [[2, "pepperoni"], [6, "pineapple"]]
pizzas_and_prices.insert(1, [2.5, "peppers"])  # will return [[2, 'pepperoni'], [2.5, 'peppers'], [6, 'pineapple']]

# APPENDING LIST TO A LIST
orders = ["daisy", "buttercup", "snapdragon", "gardenia", "lily"]
new_orders = ["lilac", "iris"]
orders_combined = orders + new_orders

broken_prices = [5, 3, 4, 5, 4] + [4]  # adding '4' instead of [4] would throw TypeError

# REMOVING LIST ELEMENTS
example_list = [1, 2, 3, 4, 5]
example_list.remove(5)

order_list = ["Celery", "Orange Juice", "Orange", "Flatbread"]
order_list.remove("Flatbread")

# ACCESSING LIST ELEMENTS - Index starts with 0,number must be int (careful when using division)
calls = ["Juan", "Zofia", "Amare", "Ezio", "Ananya"]
print(calls[2])  # will print "Amare"

# ACCESSING LIST ELEMENTS - 0 INDEX
calls = ["Juan", "Zofia", "Amare", "Ezio", "Ananya"]
print(calls[0])  # will print "Juan"
print(calls[5])  # will throw an IndexError

# ACCESSING LIST ELEMENTS - NEGATIVE INDEX
pancake_recipe = ["eggs", "flour", "butter", "milk", "sugar", "love"]
print(pancake_recipe[-1])  # will print love

# MODIFYING LIST ELEMENTS
garden_waitlist = ["Jiho", "Adam", "Sonny", "Alisha"]
garden_waitlist[1] = "Calla"  # will modify "Adam" into "Calla"

# TWO DIMENSIONAL LISTS
size = [["Jenny", 61], ["Alexus", 70], ["Sam", 67], ["Grace", 64], ["Vik", 68]]
ages = [["Aaron", 15], ["Dhruti", 16]]

# ACCESSING TWO DIMENSIONAL LISTS
heights_2d = [["Noelle", 61], ["Ali", 70], ["Sam", 67]]
noelles_height = heights_2d[0][1]
print(noelles_height)  # will print 61
noelles_height = heights_2d[-1][-1]
print(noelles_height)  # will print 67

# MODIFYING 2D LISTS
incoming_class = [["Kenny", "American", 9], ["Tanya", "Russian", 9], ["Madison", "Indian", 7]]
incoming_class[2][2] = 8  # will modify 7 into 8
incoming_class[-3][-3] = "Ken"  # will modify Kenny into Ken

# APPENDING TO 2D LIST
# list.append([value, value])

# INSERTING TO 2D LIST ELEMENT TO SPECIFIC INDEX
pizzas_and_prices = [[2, "pepperoni"], [6, "pineapple"]]
pizzas_and_prices.insert(1, [2.5, "peppers"])  # will return [[2, 'pepperoni'], [2.5, 'peppers'], [6, 'pineapple']]

# REMOVING ELEMENTS FROM 2D LISTS
customer_data = [["Ainsley", "Small", True], ["Ben", "Large", False], ["Chani", "Medium", True],
                 ["Depak", "Medium", False]]
customer_data[1].remove(False)  # removes False from "Ben", "Large"

# APPENDING 2D LIST TO 2D LIST
customer_data = [["Ainsley", "Small", True], ["Ben", "Large", False], ["Chani", "Medium", True],
                 ["Depak", "Medium", False]]
customer_data_final = customer_data + [["Amit", "Large", True], ["Karim", "X-Large", False]]