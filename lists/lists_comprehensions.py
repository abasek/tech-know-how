# LIST COMPREHENSION
# new_list = [<expression> for <element> in <collection>]
numbers = [2, -1, 79, 33, -45]
doubled = []
for number in numbers:
    doubled.append(number * 2)  # regular way
doubled = [num * 2 for num in numbers]  # list comprehension

# LIST COMPREHENSION WITH IF
numbers = [2, -1, 79, 33, -45]
only_negative_doubled = []
for num in numbers:
    if num < 0:
        only_negative_doubled.append(num * 2)  # regular way
negative_doubled = [num * 2 for num in numbers if num < 0]  # list comprehension


# LIST COMPREHENSION WITH NESTED LOOP
my_list = []
for x in [20, 40, 60]:
    for y in [2, 4, 6]:
        my_list.append(x * y)
my_list = [x * y for x in [20, 40, 60] for y in [2, 4, 6]]

# LIST COMPREHENSION IF ELSE
obj = ["Even" if i % 2 == 0 else "Odd" for i in range(10)]

# CREATING RANDOM COMPREHENSIVE LIST
import random
random_list = [random.randint(1, 100) for i in range(101)]

# FIND ALL NUMBERS FROM 1-100 THAT ARE DIVIDABLE BY 7
div7 = [number for number in range(0, 1000) if number % 7 == 0]
print(div7)

# COUNT THE NUMBER OF SPACES IN A STRING
print(len([letter for letter in "sijadijsdajda sij dja  " if letter == " "]))
