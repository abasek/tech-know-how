# ADDING SIZE OF THE LIST TO THE END OF THE LIST
def append_size(lst):
    lst.append(len(lst))
    return lst


print(append_size([23, 42, 108]))


# ADD SUM OF LAST 2 ELEMENTS OF THE LIST TO END OF THE LIST. REPEAT 3 TIMES
def append_sum(lst):
    lst.append(lst[-1] + lst[-2])
    lst.append(lst[-1] + lst[-2])
    lst.append(lst[-1] + lst[-2])
    return lst


# RETURNS TRUE IF ITEM APPEARS IN THE LIST MORE THAN N TIMES
def more_than_n(lst, item, n):
    if lst.count(item) > n:
        return True
    else:
        return False


# COMBINING TWO LISTS INTO ONE NEW AND SORTING RESULTS
def combine_sort(lst1, lst2):
    combined = lst1 + lst2
    return sorted(combined)


# RETURNING EVERY THIRD NUMBER fROM N TO 100
def every_three_nums(start):
    return list(range(start, 101, 3))


# REMOVE MIDDLE BETWEEN START AND END (INCLUSIVE)
def remove_middle(lst, start, end):
    return lst[:start] + lst[end+1:]


# RETURNING THE MORE FREQUENT ITEM
def more_frequent_item(lst, item1, item2):
    if lst.count(item1) >= lst.count(item2):
        return item1
    else:
        return item2


# DOUBLING THE INDEX
def double_index(lst, index):
    lst[index] = lst[index] * 2
    return lst


# RETURNING MIDDLE ITEM IF ODD NUMBER OF ELEMENTS IN LST, OTHERWISE AVERAGE OF THE 2 IN THE MIDDLE
def middle_element(lst):
    if len(lst) % 2 == 0:
        return (lst[int(len(lst) / 2)] + lst[int(len(lst) / 2) - 1]) / 2
    else:
        return lst[int(len(lst) / 2)]


print(middle_element([5, 2, -10, -4, 4, 5]))
print(middle_element([5, 2, -10, 7, -4, 4, 5]))
