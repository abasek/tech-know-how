# METHODS AND FUNCTIONS BELOW:
# .count() - counts the number of occurrences of an element in a list
# .insert() - inserts an element into a specific index of a list
# .pop() - removes an element from a specific index or from the end of a list
# range() - creates a sequence of integers
# len() - get he length of a list
# .sort() - sort directly but doesn't save or return any value
# sorted() - creates a new sorted list
# zip() - creates 2D list from 2 regular lists (NOT TUPLETS?)
# sum() - adds up all the number in the list

# INSERTING A LIST ITEM BY INDEX
store_line = ["Karla", "Maxium", "Martim", "Isabella"]
store_line.insert(2, "Vikor")  # index, value. New value will replace an index of an old
print(store_line)  # returns  ['Karla', 'Maxium', 'Vikor', 'Martim', 'Isabella']

# REMOVING A LIST ITEM BY INDEX
data_science_topics = ["Machine Learning", "SQL", "Pandas", "Algorithms", "Statistics", "Python 3"]
data_science_topics.pop()  # will remove the last element
data_science_topics.pop(3)  # will remove "Algorithms"

# CREATING LISTS BASED ON RANGE - last number not included
zero_to_seven_list = list(range(0, 8))  # will create a list [0, 1, 2, 3, 4, 5, 6, 7]
print(zero_to_seven_list)

zero_to_eight = range(0, 9)  # list creation happens before printing
print(list(zero_to_eight))  # will return [0, 1, 2, 3, 4, 5, 6, 7, 8]

skips_every_two = range(2, 9, 2)
print(list(skips_every_two))  # will return [2, 4, 6, 8]

my_range3 = range(1, 100, 10)
print(list(my_range3))  # will return [1, 11, 21, 31, 41, 51, 61, 71, 81, 91]

# LENGTH - FINDING THE NUMBER OF ELEMENTS OF A LIST
long_list = [1, 5, 6, 7, -23, 69.5, True, "very", "long", "list"]
long_list_len = len(long_list)
print(long_list_len)  # will return 10

big_range = range(2, 3000, 100)
big_range_length = len(big_range)
print(big_range_length)  # will return 30

# SLICING LISTS - based on index, last element not included
suitcase = ["shirt", "shirt", "pants", "pants", "pajamas", "books"]
beginning = suitcase[0:2]  # will return ['shirt', 'shirt']

suitcase = ["shirt", "shirt", "pants", "pants", "pajamas", "books"]
middle = suitcase[2:4]  # will return ['pants', 'pants']

# SLICING N ELEMENTS OF A LIST
fruits = ["apple", "cherry", "pineapple", "orange", "mango"]
print(fruits[:3])  # ['apple', 'cherry', 'pineapple'] First 3
print(fruits[2:])  # ['pineapple', 'orange', 'mango']
print(fruits[-2:])  # ['orange', 'mango'] Last 2
print(fruits[:-3])  # ['apple', 'cherry', 'pineapple', 'orange'] All but the 3 last
print(fruits[2:3])  # ['pineapple]

# COUNTING SPECIFIC ELEMENTS OF A LIST
votes = ["Jake", "Jake", "Laurie", "Laurie", "Laurie", "Jake", "Jake", "Jake", "Laurie", "Cassie", "Cassie", "Jake",
         "Jake", "Cassie", "Laurie", "Cassie", "Jake", "Jake", "Cassie", "Laurie"]
jake_votes = votes.count("Jake")
print(jake_votes)

number_collection = [[100, 200], [100, 200], [475, 29], [34, 34]]
num_pairs = number_collection.count([100, 200])
print(num_pairs)  # returns 2

# SORTING A LIST using .sort()- modifies the list directly, not return anything
addresses = ["221 B Baker St.", "42 Wallaby Way", "12 Grimmauld Place"]
addresses.sort()
print(addresses)  # returns ['12 Grimmauld Place', '221 B Baker St.', '42 Wallaby Way']

names = ["Ron", "Hermione", "Harry", "Albus", "Sirius"]
names.sort(reverse=True)
print(names)  # returns ['Sirius', 'Ron', 'Hermione', 'Harry', 'Albus']

cities = ["London", "Paris", "Rome", "Los Angeles", "New York"]
sorted_cities = cities.sort()
print(sorted_cities)  # will return None as function does not return anything

# SORTING A LIST using sorted()- generates a new list (previous .sort() modifies current one)
games = ["Portal", "Minecraft", "Pacman", "Tetris", "The Sims", "Pokemon"]
games_sorted = sorted(games)
print(games_sorted)  # returns ['Minecraft', 'Pacman', 'Pokemon', 'Portal', 'Tetris', 'The Sims']

# COMBINING TWO REGULAR LISTS INTO 2D LIST USING ZIP (Don't want tuplets? - use '+' to combine)
names = ["Jenny", "Alexus", "Sam", "Grace"]
heights = [61, 70, 67, 64]
names_and_heights = list(zip(names, heights))
print(names_and_heights)  # returns [('Jenny', 61), ('Alexus', 70), ('Sam', 67), ('Grace', 64)]

n_and_h = zip(names, heights)  # returns <zip object at 0x7f1631e86b48>

# SUM ALL THE LIST VALUES - STARTING POINT 0 IF NOT GIVEN AS 2 ARGUMENT
numbers = [1, 2, 3, 4, 5, 1, 4, 5]
sum1 = sum(numbers)  # returns 25
sum2 = sum(numbers, 10)  # returns 35
