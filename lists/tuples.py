# CREATING A TUPLES - immutable comparing to list - not possible to add, remove, sort etc.

my_info = ('Adam', 32, 'Tester')
print(my_info)
print(my_info[1])

# TRY TO CHANGE
# my_info[0] = 'Michal'  # returns TypeError: 'tuple' object does not support item assignment

# UNPACKING TUPLES
my_info = ('Adam', 32, 'Tester')
name, age, occupation = my_info  # order matters
print(name)  # Returns 'Adam'

# ONE ELEMENT TUPLE
one_element_tuple = (4,)  # because parenthesis are used for math mostly
print(one_element_tuple)  # returns (4,) which is a one element tuple
