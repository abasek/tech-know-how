# Encapsulation is a way to can restrict access to methods and variables from outside of class.
# Whenever we are working with the class and dealing with sensitive data, providing access to
# all variables used within the class is not a good choice.

# ACCESS MODIFIERS:

# class Employee:
# def __init__(self, name, salary):
#     self.name = name - Public Member (accessible within or outside of a class)
#     self._project = project - Protected Member (accessible within the class and it's subclasses
#     self.__salary = salary - Private Member (accessible only within a class)

# 1. PUBLIC MEMBER
class Employee:
    # constructor
    def __init__(self, name, salary):
        # public data members
        self.name = name
        self.salary = salary

    # public instance methods
    def show(self):
        # accessing public data member
        print("Name: ", self.name, 'Salary:', self.salary)

# creating object of a class
emp = Employee('Jessa', 10000)

# accessing public data members
print("Name: ", emp.name, 'Salary:', emp.salary)  # returns Name:  Jessa Salary: 10000

# calling public method of the class
emp.show()  # returns Name:  Jessa Salary: 10000

# 2. PRIVATE MEMBER
# Private members are accessible only within the class, and we can’t access them
# directly from the class objects.

class Employee:
    # constructor
    def __init__(self, name, salary):
        # public data member
        self.name = name
        # private member
        self.__salary = salary

# creating object of a class
emp = Employee('Jessa', 10000)

# accessing private data members
print('Salary:', emp.__salary)  # returns AttributeError: 'Employee' object has no attribute '__salary'

# ACCESSING PRIVATE MEMBER OUTSIDE OF A CLASS USING AN INSTANCE METHOD
class Employee:
    # constructor
    def __init__(self, name, salary):
        # public data member
        self.name = name
        # private member
        self.__salary = salary

    # public instance methods
    def show(self):
        # private members are accessible from a class
        print("Name: ", self.name, 'Salary:', self.__salary)

# creating object of a class
emp = Employee('Jessa', 10000)

# calling public method of the class
emp.show()  # returns Name: Jessa Salary: 10000

# NAME MANGLING TO ACCESS PRIVATE MEMBERS
class Employee:
    # constructor
    def __init__(self, name, salary):
        # public data member
        self.name = name
        # private member
        self.__salary = salary

# creating object of a class
emp = Employee('Jessa', 10000)

print('Name:', emp.name)  # returns Name: Jessa
# direct access to private member using name mangling
print('Salary:', emp._Employee__salary)  # returns Salary: 10000

# 3. PROTECTED MEMBER
# Protected data members are used when you implement inheritance and want to allow data members
# access to only child classes.

# base class
class Company:
    def __init__(self):
        # Protected member
        self._project = "NLP"

# child class
class Employee(Company):
    def __init__(self, name):
        self.name = name
        Company.__init__(self)

    def show(self):
        print("Employee name :", self.name)
        # Accessing protected member in child class
        print("Working on project :", self._project)

c = Employee("Jessa")
c.show()  # returns:
# Employee name : Jessa
# Working on project : NLP

# Direct access protected data member
print('Project:', c._project)  # returns Project: NLP

# GETTERS AND SETTERS
class Student:
    def __init__(self, name, age):
        # private member
        self.name = name
        self.__age = age

    # getter method
    def get_age(self):
        return self.__age

    # setter method
    def set_age(self, age):
        self.__age = age

stud = Student('Jessa', 14)

# retrieving age using getter
print('Name:', stud.name, stud.get_age())  # returns Name: Jessa 14

# changing age using setter
stud.set_age(16)

# retrieving age using getter
print('Name:', stud.name, stud.get_age())  # returns Name: Jessa 16

# INFO HIDING AND CONDITIONAL LOGIC FOR SETTING AN OBJECT ATTRIBUTES
class Student:
    def __init__(self, name, roll_no, age):
        # private member
        self.name = name
        # private members to restrict access
        # avoid direct data modification
        self.__roll_no = roll_no
        self.__age = age

    def show(self):
        print('Student Details:', self.name, self.__roll_no)

    # getter methods
    def get_roll_no(self):
        return self.__roll_no

    # setter method to modify data member
    # condition to allow data modification with rules
    def set_roll_no(self, number):
        if number > 50:
            print('Invalid roll no. Please set correct roll number')
        else:
            self.__roll_no = number

jessa = Student('Jessa', 10, 15)

# before Modify
jessa.show()  # returns Student Details: Jessa 10
# changing roll number using setter
jessa.set_roll_no(120)  # returns Invalid roll no. Please set correct roll number


jessa.set_roll_no(25)
jessa.show()  # returns Student Details: Jessa 25

# * https://pynative.com/python-encapsulation/

