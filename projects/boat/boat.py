class DriveBot:
    all_disabled = False
    latitude = -999999
    longitude = -999999
    robot_count = 0

    def __init__(self, motor_speed=0, direction=180, sensor_range=10):
        self.motor_speed = motor_speed
        self.direction = direction
        self.sensor_range = sensor_range
        DriveBot.robot_count += 1
        self.id = DriveBot.robot_count

    def control_bot(self, new_speed, new_direction):
        self.motor_speed = new_speed
        self.direction = new_direction

    def adjust_sensor(self, new_sensor_range):
        self.sensor_range = new_sensor_range


# CHANGING BOAT PARAMETERS
robot_1 = DriveBot()
robot_1.motor_speed = 5
robot_1.direction = 90
robot_1.sensor_range = 10

# CHANGING BOAT PARAMETERS BY METHOD
robot_2 = DriveBot()
robot_1.control_bot(10, 180)
robot_1.adjust_sensor(20)

# CREATING BOATS WITH PARAMETERS
robot_3 = DriveBot(35, 75, 25)
robot_4 = DriveBot(20, 60, 10)

# CHANGING ALL BOAT PARAMETERS AT ONCE
DriveBot.longitude = 50
DriveBot.latitude = -50
DriveBot.all_disabled = True

# CHECKING THE INCREMENTATION
print(robot_1.id)
print(robot_2.id)
print(robot_3.id)
