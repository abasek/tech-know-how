class Car:
    def __init__(self, brand_name):
        self.total_distance = 0
        self.brand = brand_name

    def drive(self, distance):
        self.total_distance += distance

    def describe_car(self):
        return f'{self.brand} has driven {self.total_distance} kms'


if __name__ == '__main__':
    car = Car('Toyota')
    car.describe_car()
    car.drive(100)
    car.describe_car()

