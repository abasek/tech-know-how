from datetime import datetime


class Order:
    def __init__(self, customer_email):
        self.customer_email = customer_email
        self.total_amount = 0
        self.items = []
        self.created = False
        self.order_id = None

    def add_item(self, item_dictionary):
        if self.created:
            print('The order is already created')
        else:
            self.items.append(item_dictionary)
            self.total_amount += item_dictionary['quantity'] * item_dictionary['price']

    def create(self):
        self.order_id = datetime.timestamp(datetime.now())
        self.created = True

    def print_bill(self):
        if not self.created:
            print('Order not yet created')
        else:
            item_list_string = '\n'.join(
                [f'- {item["name"]}, {item["price"]} x {item["quantity"]}' for item in self.items])
            print(
                f'ORDER ID: {self.order_id} \nCustomer: {self.customer_email} \nItems: \n{item_list_string} \nTotal Price: {self.total_amount}')


if __name__ == '__main__':
    tikka = {"name": "Chicken Tikka", "price": 23.00, "quantity": 2}
    nachos = {"name": "Nachos", "price": 12.00, "quantity": 1}
    sushi = {"name": "Sushi", "price": 23.00, "quantity": 2}

    my_order = Order('adrian.gonciarz@kitopi.com')
    my_order.add_item(tikka)
    my_order.add_item(nachos)
    my_order.add_item(sushi)
    my_order.create()
    my_order.print_bill()
