class Business:
    def __init__(self, name, franchises):
        self.name = name
        self.franchises = franchises


class Franchise:
    def __init__(self, address, menus):  # menus are objects of a class Menu
        self.address = address
        self.menus = menus

    def __repr__(self):
        return "The address of the restaurant is {}".format(self.address)

    def available_menus(self, time):
        available_menu = []
        for menu in self.menus:
            if menu.start_time <= time <= menu.end_time:  # referring to variable of on object from Menu class
                available_menu.append(menu)
        return available_menu


class Menu:
    def __init__(self, name, items, start_time, end_time):
        self.name = name
        self.items = items
        self.start_time = start_time
        self.end_time = end_time

    def __repr__(self):
        return "{} menu available from {} to {}".format(self.name, self.start_time, self.end_time)

    def calculate_bill(self, purchased_items):
        total_value = 0
        for item in purchased_items:
            total_value += self.items[item]
        return total_value


brunch = Menu("brunch", {
    'pancakes': 7.50, 'waffles': 9.00, 'burger': 11.00, 'home fries': 4.50, 'coffee': 1.50, 'espresso': 3.00,
    'tea': 1.00, 'mimosa': 10.50, 'orange juice': 3.50}, 11, 16)

early_bird = Menu("Early-bird", {
    'salumeria plate': 8.00, 'salad and breadsticks (serves 2, no refills)': 14.00, 'pizza with quattro formaggi': 9.00,
    'duck ragu': 17.50, 'mushroom ravioli (vegan)': 13.50, 'coffee': 1.50, 'espresso': 3.00}, 15, 18)

dinner = Menu("Dinner", {
    'crostini with eggplant caponata': 13.00, 'caesar salad': 16.00, 'pizza with quattro formaggi': 11.00,
    'duck ragu': 19.50, 'mushroom ravioli (vegan)': 13.50, 'coffee': 2.00, 'espresso': 3.0
}, 17, 23)

kids = Menu("Kids", {
    'chicken nuggets': 6.50, 'fusilli with wild mushrooms': 12.00, 'apple juice': 3.00
}, 11, 21)

aperas_menu = Menu("aperas", {
    'arepa pabellon': 7.00, 'pernil arepa': 8.50, 'guayanes arepa': 8.00, 'jamon arepa': 7.50
}, 10, 20)

flagship_store = Franchise("1232 West End Road", [brunch, early_bird, dinner, kids])

new_installment = Franchise("12 East Mulberry Street", [brunch, early_bird, dinner, kids])

aperas_place = Franchise("189 Fitzgerald Avenue", [aperas_menu])

basta_fazoolin = Business("Basta Fazoolin with my Heart", [flagship_store, new_installment])

arepa = Business("'Take a' Arepa", [aperas_place])

print(brunch)
print(brunch.calculate_bill(["pancakes", "home fries", "coffee"]))
print(early_bird.calculate_bill(["salumeria plate", "mushroom ravioli (vegan)"]))
print(flagship_store)
print(flagship_store.available_menus(17))
print(arepa.franchises[0])
print(arepa.franchises[0].menus[0])
