class RestaurantClosedError(Exception):
    pass


class NoFreeTablesError(Exception):
    pass


class Restaurant:

    def __init__(self, name, number_of_tables):
        self.name = name
        self.number_of_tables = number_of_tables
        self.number_of_free_tables = self.number_of_tables
        self.opened = False

    def __repr__(self):
        status = 'open' if self.opened else 'closed'
        return f'{self.name} is {status} and has {self.number_of_tables} tables out of which {self.number_of_free_tables} are free'

    def open(self):
        self.opened = True

    def close(self):
        self.opened = False
        self.number_of_free_tables = self.number_of_tables

    def reserve_table(self):
        if not self.opened:
            raise RestaurantClosedError('Restaurant is closed')
        elif self.number_of_free_tables == 0:
            raise NoFreeTablesError('There are no free tables')
        else:
            self.number_of_free_tables -= 1
            return self.number_of_free_tables


if __name__ == '__main__':
    hindus = Restaurant('Mr. India', 2)
    hindus.open()
    hindus.reserve_table()
    hindus.close()
