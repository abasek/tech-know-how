from Python.projects.restaurant.restaurant import Restaurant, RestaurantClosedError, NoFreeTablesError
import pytest as pytest


@pytest.fixture(scope="module")
def restaurant():
    hindus = Restaurant('Mr. India', 2)
    yield hindus
    hindus.close()


def test_initial_restaurant_values(restaurant):
    assert restaurant.name == 'Mr. India'
    assert restaurant.number_of_tables == 2
    assert restaurant.number_of_free_tables == restaurant.number_of_tables
    assert not restaurant.opened


def test_initial_message(restaurant):
    assert str(restaurant) == "Mr. India is closed and has 2 tables out of which 2 are free"


def test_opening_restaurant(restaurant):
    restaurant.open()
    assert restaurant.opened


def test_closing_restaurant(restaurant):
    restaurant.open()
    restaurant.reserve_table()
    restaurant.close()
    assert not restaurant.opened
    assert restaurant.number_of_free_tables == restaurant.number_of_tables


def test_reserving_table_when_restaurant_closed(restaurant):
    with pytest.raises(RestaurantClosedError):
        restaurant.reserve_table()


def test_reserving_table_when_restaurant_opened(restaurant):
    restaurant.open()
    restaurant.reserve_table()
    assert restaurant.number_of_free_tables == 1


def test_reserving_more_tables_than_available(restaurant):
    restaurant.close()
    restaurant.open()
    restaurant.reserve_table()
    restaurant.reserve_table()
    with pytest.raises(NoFreeTablesError):
        restaurant.reserve_table()

