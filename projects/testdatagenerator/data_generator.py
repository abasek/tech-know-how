import random
from datetime import date

female_fnames = ['Kate', 'Agnieszka', 'Anna', 'Maria', 'Joss', 'Eryka']
male_fnames = ['James', 'Bob', 'Jan', 'Hans', 'Orestes', 'Saturnin']
surnames = ['Smith', 'Kowalski', 'Yu', 'Bona', 'Muster', 'Skinner', 'Cox', 'Brick', 'Malina']
countries = ['Poland', 'United Kingdom', 'Germany', 'France', 'Other']
list_of_dict = []

for item in range(0, 5):
    dict = {}
    dict["firstname"] = random.choice(female_fnames)
    dict["lastname"] = random.choice(surnames)
    dict["country"] = random.choice(countries)
    dict["email"] = dict["firstname"].lower() + "." + dict["lastname"].lower() + "@example.com"
    dict["age"] = random.randint(5, 45)
    dict["adult"] = True if dict["age"] >= 18 else False
    dict["birth_year"] = date.today().year - dict["age"]
    list_of_dict.append(dict)

for item in range(6, 11):
    dict = {}
    dict["firstname"] = random.choice(male_fnames)
    dict["lastname"] = random.choice(surnames)
    dict["country"] = random.choice(countries)
    dict["email"] = dict["firstname"].lower() + "." + dict["lastname"].lower() + "@example.com"
    dict["age"] = random.randint(5, 45)
    dict["adult"] = True if dict["age"] >= 18 else False
    dict["birth_year"] = date.today().year - dict["age"]  # da się datę poprawić żeby miesiące uwzględniała?
    list_of_dict.append(dict)

# print(list_of_dict)
if __name__ == '__main__':
    for item in list_of_dict: print("Hi! I\'m {} {}. I come from {} and I was born in {}".format(item["firstname"],
item["lastname"], item["country"], item["birth_year"]))
