import random
import json


def get_new_user_cart(email):
    """
    Returns cart as dictionary with initial data
    {
       'email': 'provided_email',
       'items': [],
       'total_price': 0.0
    }
    """
    new_user_cart = {"email": email, "items": [], "total_price": 0.0}
    return new_user_cart


def generate_cart_item():
    """Returns random cart item in the form of dictionary
    {
       "name": "Banana",
       "item_price": 2.99,
       "quantity": 3
    }
    """
    names = ["Banana", "Kiwi", "Strawberry", "Gunwo"]
    random_cart = {"name": random.choice(names), "price": round(random.random(), 2),
                   "quantity": random.randint(1, 10)}
    return random_cart


def generate_cart_items(number_of_items):
    """
    Returns a list of random cart items of a length of number_of_items
    """
    return [generate_cart_item() for number in range(number_of_items)]


def calculate_item_price(item_dictionary):
    """
    Returns the price of item by multiplying item_price * quantity
    """
    total = 0
    for item in item_dictionary["items"]:
        total += item["price"] * item["quantity"]
    return round(total, 2)


def save_cart_to_file(filename, cart_dictionary):
    with open(filename, 'w') as f:
        json_string = json.dumps(cart_dictionary, indent=4)
        f.write(json_string)


if __name__ == '__main__':
    user_cart = get_new_user_cart('adrian@example.com')
    user_cart["items"] = generate_cart_items(5)
    user_cart["total_price"] = calculate_item_price(user_cart)
    save_cart_to_file('cart.json', user_cart)

print(get_new_user_cart("adam.basek@gmail.com"))
print(generate_cart_item())
print(generate_cart_items(3))
# print(calculate_item_price({'name': 'Banana', 'price': 3.44, 'quantity': 3}))  #wrong args
