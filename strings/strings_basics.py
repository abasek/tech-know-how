# STRINGS ARE LISTS
my_name = "Adam"
first_initial = my_name[0]


# CREATING LOGIN OR PASSWORD - NOT REALLY SAFE :)
def account_generator(first_name_t, last_name):
    return first_name_t[0:3] + last_name[0:3]


# STRINGS ARE IMMUTABLE
first_name_b = "Bob"
first_name_b[0] = "R"  # will return TypeError

# TO CHANGE THE STRING YOU NEED TO CREATE A NEW ONE
first_name = "Bob"
fixed_first_name = "R" + first_name[1:]

# ESCAPE CHARACTER
text1 = "they call me\"crazy\"91"  # they call me"crazy"91
text2 = "I\'m Adam"  # I'm Adam


# ITERATING THROUGH STRINGS
def print_each_letter(word):
    for letter in word:
        print(letter)


print(print_each_letter("Adam"))

# IN SYNTAX IN STRINGS (AND LISTS?)
# letter in word  # returns True or False
print("e" in "blueberry")  # returns True
print("a" in "blueberry")  # returns  False
