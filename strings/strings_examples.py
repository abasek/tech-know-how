# RETURNING THE COMMON LETTERS IN 2 STRINGS USING IN SYNTAX
def common_letters(string_one, string_two):
    list_a = []
    for i in string_one:
        if i in string_two and i not in list_a:
            list_a.append(i)
    return list_a


# COUNTING UNIQUE LETTERS
letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"


def unique_english_letters(word):
    unique = []
    for i in word:
        if i in letters and i not in unique:
            unique.append(i)
    return len(unique)


print(unique_english_letters("mississippi"))
print(unique_english_letters("Apple"))


# NUMBER OF TIMES A LETTER APPEARS IN A WORD
def count_char_x(word, x):
    count = 0
    for letter in word:
        if letter == x:
            count += 1
    return count


print(count_char_x("mississippi", "s"))
print(count_char_x("mississippi", "m"))


# NUMBER OF TIMES A STRING APPEARS IN ANOTHER STRING
def count_multi_char_x(word, x):
    split = word.split(x)
    return len(split) - 1


print(count_multi_char_x("mississippi", "iss"))
print(count_multi_char_x("apple", "pp"))


# RETURN SUBSTRING BETWEEN A AND B, IF A OR B NOT IN STRING RETURN WORD
def substring_between_letters(word, start, end):
    if start in word and end in word:
        return word[word.index(start) + 1:word.index(end)]
    else:
        return word


# RETURN SUBSTRING BETWEEN A AND B, IF A OR B NOT IN STRING RETURN WORD USING .FIND
def substring_between_letters_2(word, start, end):
    start_ind = word.find(start)
    end_ind = word.find(end)
    if start_ind > -1 and end_ind > -1:
        return word[start_ind + 1:end_ind]
    return word


print(substring_between_letters_2("apple", "p", "e"))
print(substring_between_letters_2("apple", "p", "c"))


# RETURN TRUE IN EVERY SINGLE WORD IN SENTENCE HAS LENGTH GREATER/EQUAL THAN NUMBER (SEE BELOW)
def x_length_words(sentence, x):
    splitted_sentence = sentence.split()
    count = 0
    for word in splitted_sentence:
        if len(word) >= x:
            count += 1
    if count == len(splitted_sentence):
        return True
    else:
        return False


print(x_length_words("i like apples", 2))
print(x_length_words("he likes apples", 2))


# RETURN TRUE IN EVERY SINGLE WORD IN SENTENCE HAS LENGTH GREATER/EQUAL THAN NUMBER - OTHER EXAMPLE
def x_length_words(sentence, x):
    words = sentence.split(" ")
    for word in words:
        if len(word) < x:
            return False
    return True


# RETURN TRUE IF SENTENCE X IN Y REGARDLESS OF THE CAPITAL LETTERS
def check_for_name(sentence, name):
    return name.lower() in sentence.lower()


# RETURN EVERY SECOND LETTER OF A WORD - RANGE
def every_other_letter(word):
    every_other = ""
    for i in range(0, len(word), 2):
        every_other += word[i]
    return every_other


# RETURN REVERSE OF THE WORD
def reverse_string(word):
    string = ""
    for i in range(0, (len(word))):
        string += word[-i - 1]
    return string


# RETURN REVERSE OF THE WORD - OTHER SOLUTION
def reverse_string_2(word):
    reverse = ""
    for i in range(len(word) - 1, -1, -1):
        # starts with last index - len -1, instead of 0: -1, increments by -1
        reverse += word[i]
    return reverse


# ADD EXCLAMATION POINT TO THE WORD UNTIL ITS LENGTH IS 20
def add_exclamation(word):
    while len(word) < 20:
        word += "!"
    return word


# CLEANING LONG STRING
daily_sales = \
    """Edith Mcbride   ;,;$1.21   ;,;   white ;,;
    09/15/17   ,Herbert Tran   ;,;   $7.29;,;
    white&blue;,;   09/15/17 ,Paul Clarke ;,;$12.52
    ;,;   white&blue ;,; 09/15/17 ,Lucille Caldwell
    ;,;   $5.13   ;,; white   ;,; 09/15/17,
    Eduardo George   ;,;$20.39;,; white&yellow
    ;,;09/15/17   ,   Danny Mclaughlin;,;$30.82;,;
    purple ;,;09/15/17 ,Stacy Vargas;,; $1.85   ;,;
    purple&yellow ;,;09/15/17,   Shaun Brock;,;
    $17.98;,;purple&yellow ;,; 09/15/17 ,
    Erick Harper ;,;$17.41;,; blue ;,; 09/15/17,
    Michelle Howell ;,;$28.59;,; blue;,;   09/15/17   ,
    Carroll Boyd;,; $14.51;,;   purple&blue   ;,;
    09/15/17   , Teresa Carter   ;,; $19.64 ;,;
    white;,;09/15/17   ,   Jacob Kennedy ;,; $11.40
    ;,; white&red   ;,; 09/15/17, Craig Chambers;,;
    $8.79 ;,; white&blue&red   ;,;09/15/17   , Peggy Bell;,; $8.65 ;,;blue   ;,; 09/15/17,   Kenneth Cunningham ;,;   $10.53;,;  green&white;,;09/15/17,   Gail Phelps   ;,;$30.52
    ;,; green&white&blue   ;,; 09/15/17 , Myrtle Morris
    ;,;   $22.66   ;,; green&white&blue;,;09/15/17"""

daily_sales_replaced = daily_sales.replace(";,;", "?")  # cleaning delimiter
daily_transactions = daily_sales_replaced.split(",")  # splitting long strings into list
daily_transactions_split = []
for i in daily_transactions:
    daily_transactions_split.append(i.split("?"))  # splitting strings in the list creating 2D list

transactions_clean = []  # structure enabling keeping 2D list with cleared data
for i in daily_transactions_split:
    transaction_clean = []
    for n in i:
        transaction_clean.append(n.strip())
        transactions_clean.append(transaction_clean)
print(transactions_clean)

# CLEANING ANOTHER STRING
highlighted_poems = "Afterimages:Audre Lorde:1997,  The Shadow:William Carlos Williams:1915, Ecstasy:Gabriela " \
                    "Mistral:1925,   Georgia Dusk:Jean Toomer:1923,   Parting Before Daybreak:An Qi:2014, The Untold " \
                    "Want:Walt Whitman:1871, Mr. Grumpledump's Song:Shel Silverstein:2004, Angel Sound Mexico " \
                    "City:Carmen Boullosa:2013, In Love:Kamala Suraiyya:1965, Dream Variations:Langston Hughes:1994, " \
                    "Dreamwood:Adrienne Rich:1987 "

highlighted_poems_list = highlighted_poems.split(",")

highlighted_poems_stripped = []
for i in highlighted_poems_list:
    highlighted_poems_stripped.append(i.strip())
highlighted_poems_details = []
for i in highlighted_poems_stripped:
    highlighted_poems_details.append(i.split(":"))

titles = []
poets = []
dates = []
for i in highlighted_poems_details:
    titles.append(i[0])
    poets.append(i[1])
    dates.append(i[2])

print(titles)
print(poets)
print(dates)
for i in range (0, len(poets)):
    print("The poem {} was published by {} in {}.".format(titles[i], poets[i], dates[i]))
