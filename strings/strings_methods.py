# METHODS  BELOW:
# .upper() > "HELLO WORLD"
# .lower() > "hello world"
# .title() > "Hello World"
# .split() > ['Hello', 'world']
# .replace('H', 'J') > "Jello world"
# .strip() - removes whitespace characters from the beginning and end
# .find() - find returns the first index value where that string is located
# ' '.join(['Hello', 'world']) > "Hello world"  # uses list
# "{} {}".format("Hello", "world") - "Hello world"

# STRING CANNOT BE CHANGED DIRECTLY
line_one = "The sky has given over"
line_one.split()
print(line_one)  # will print "Ske sky has given over"

# BUT CAN BE PRINTED WITH CHANGE
line_one = "The sky has given over"
print(line_one.split())  # will print ['The', 'sky', 'has', 'given', 'over']

# SPLITTING THE STRING BY SPACE
line_one = "The sky has given over"
line_one_word = line_one.split()  # will return ['The', 'sky', 'has', 'given', 'over']

# SPLITTING THE STRING BY COMA
authors = "Audre Lorde,Gabriela Mistral,Jean Toomer,An Qi"
author_names = authors.split(',')
print(author_names)  # returns ['Audre Lorde', 'Gabriela Mistral', 'Jean Toomer', 'An Qi']

# SPLITTING STRINGS IN A LIST AND APPENDING LAST WORD TO ANOTHER LIST
author_names = ['Audre Lorde', 'Gabriela Mistral', 'Jean Toomer', 'An Qi']
author_last_names = []
for name in author_names:
    author_last_names.append(name.split()[-1])
print(author_last_names)  # returns ['Lorde', 'Mistral', 'Toomer', 'Qi']

# SPLITTING STRINGS BY NEWLINE
smooth_chorus = \
    """And "life"
    I would
    I could
    Because"""
chorus_lines = smooth_chorus.split('\n')  # returns ['And "life"', 'I would', 'I could', 'Because']

# SPLITTING STRINGS BY TABS
"String".split('\t')

# JOINING LIST INTO A STRING
reapers_line_one_words = ["Black", "reapers", "with", "the", "sound", "of", "steel", "on", "stones"]
reapers_line_one = ' '.join(reapers_line_one_words)
print(reapers_line_one)  # returns Output: Black reapers with the sound of steel on stones

# JOINING WITH COMA
santana_songs = ['Oye Como Va', 'Smooth', 'Black Magic Woman', 'Samba Pa Ti', 'Maria Maria']
santana_songs_csv = ','.join(santana_songs)

# JOINING INTO LYRICS
winter_trees_lines = ['All the complicated details', 'of the attiring and']
winter_trees_full = '\n'.join(winter_trees_lines)  # returns
# All the complicated details
# of the attiring and

# STRIPPING WHITESPACES
featuring = "           rob thomas                 "
print(featuring.strip())  # returns  'rob thomas'

# STRIPPING SPECIFIC CHARACTER (WITHOUT WHITESPACES ANY MORE)
featuring = "!!!rob thomas       !!!!!"
print(featuring.strip('!'))  # returns 'rob thomas       '

# CLEANING STRINGS IN A LIST USING STRIP
love_maybe_lines = ['Always    ', '     in the middle of our bloodiest battles  ']
love_maybe_lines_stripped = []
for i in love_maybe_lines:
    love_maybe_lines_stripped.append(i.strip())

# REPLACING PART OF THE STRING
with_spaces = "You got the kind of loving that can be so smooth"
with_underscores = with_spaces.replace(' ', '_')
print(with_underscores)  # returns  'You_got_the_kind_of_loving_that_can_be_so_smooth'

# FINDING INDEX OF THE STRINGS (INDEX OF THE FIRST LETTER
god_wills_it_line_one = "The very earth will disown you"
disown_placement = god_wills_it_line_one.find("disown")
print(disown_placement)  # returns 20


# FORMATTING USING PARAMETERS
def poem_title_card(title, poem):
    return "The poem \"{}\" is written by {}.".format(title, poem)


# FORMATTING USING PARAMETERS IN MORE LEGIBLE WAY
def favorite_song_statement(song, artist):
    return "My favorite song is {song} by {artist}.".format(artist=artist, song=song)


# FORMATTING USING THE BEST WAY F'STRING
def favorite_song(song, artist):
    return f'My favorite song is {song} by {artist}.'


# FORMATTING USING F'STRING
my_list = ['Adam', 'Adrian']
my_dict = {'name': 'Adam'}
print(f'My name is {my_list[0]}')
print(f'My name is {my_dict["name"]}')
print(f'Ten times eight is {10 * 8}')

# FORMATTING USING F'STRING IN LONG DOCSTRING - LONG TEXT
city = 'Warszawa'
job = 'IT'
moj_opis = f"""I live in {city}.
I work in {job}
"""  # empty line, if we don't want it we must move it to the previous line
