import os

import pytest
from selenium import webdriver
from selenium.webdriver.remote.webdriver import WebDriver
from utils import config
from utils.helpers import take_screenshot
from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.firefox import GeckoDriverManager


@pytest.fixture(params=["chrome"])  # "firefox"
def driver(request, screenshots_directory) -> WebDriver:
    if request.param == 'firefox':
        driver = webdriver.Firefox(executable_path=GeckoDriverManager().install())
    else:
        driver = webdriver.Chrome(executable_path=ChromeDriverManager().install())
    driver.get(config.url)
    driver.maximize_window()
    yield driver
    if request.node.rep_call.failed:
        take_screenshot(driver, screenshots_directory)
        print("executing test failed", request.node.nodeid)
    driver.quit()


@pytest.fixture(scope='session', autouse=True)
def screenshots_directory() -> str:
    path = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', 'screenshots'))
    if not os.path.exists(path):
        os.makedirs(path)
    return path


@pytest.hookimpl(tryfirst=True, hookwrapper=True)
def pytest_runtest_makereport(item):
    outcome = yield
    rep = outcome.get_result()
    setattr(item, "rep_" + rep.when, rep)
