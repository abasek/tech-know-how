import os
import time
from time import sleep


def take_screenshot(driver, screenshots_directory):
    sleep(1)
    file_name = f'{time.time()}.png'
    file_path = os.path.abspath(os.path.join(screenshots_directory, file_name))
    driver.save_screenshot(file_path)
