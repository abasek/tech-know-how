import os

from selenium import webdriver
from selenium.webdriver.common.by import By
from webdriver_manager.firefox import GeckoDriverManager
import pytest


@pytest.fixture
def driver():
    driver = webdriver.Firefox(executable_path=GeckoDriverManager().install())
    driver.get('https://onet.pl')
    yield driver
    driver.quit()

# UPLOADING A FILE
# 1st option - the most common option
# driver.find_element(element).send_keys(file_path)

# 2nd option - use ActionChains (but what when file explorer is open???)
# element = self.driver.find_element(*self.choose_file_button)
# ActionChains(self.driver).move_to_element(element).click().perform()  # will open a windows file explorer

# 3rd option - find an input element
# self.driver.find_element(*self.choose_file_input).send_keys(file_path)

# File path to the above in windows
file_path = os.path.abspath(
            "C:\\Users\\Admin\\PycharmProjects\\seleniumAutomation\\utils\\testfiles\\fileforupload.txt")


# EXPLICIT WAIT
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.ID, "myDynamicElement")))

# Making it simpler
wait = WebDriverWait(driver, 10)
element_wait = wait.until(EC.element_to_be_clickable((By.ID, 'someid')))

# Custom wait - https://selenium-python.readthedocs.io/waits.html#explicit-waits


# IMPLICIT WAIT - to be added to driver in conftest
driver.implicitly_wait(10)  # seconds


# SAVE SCREENSHOT
driver.save_screenshot("add_to_cart")  # see also fixture examples


# IFRAME
# Wait for iframe to load
iframe_selector = (By.CSS_SELECTOR, "iframe.fancybox-iframe")
iframe_element = WebDriverWait(driver, 20).until(EC.presence_of_element_located(iframe_selector))

# Switch to iframe
driver.switch_to.frame(iframe_element)

# Actions
driver.find_element(By.CSS_SELECTOR, 'i.icon-plus').click()
driver.find_element(By.CSS_SELECTOR, '#add_to_cart').click()

driver.switch_to.default_content()


# DRAG AND DROP
from selenium.webdriver import ActionChains
element = driver.find_element(By.NAME, "source")
target = driver.find_element(By.NAME, "target")

action_chains = ActionChains(driver)
action_chains.drag_and_drop(element, target).perform()

# Moving between windows and frames
# Popup dialogs
# Navigation: history and location
# Cookies
# https://selenium-python.readthedocs.io/navigating.html


# HOW TO CHECK WHAT ELEMENT IS BEING CLICKED
# base64 decode image - to be found online and copy paste base64 element from debugger
